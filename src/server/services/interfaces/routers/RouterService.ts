import * as Router from "koa-router";

export interface RouterService {
    register(router: Router);
}
