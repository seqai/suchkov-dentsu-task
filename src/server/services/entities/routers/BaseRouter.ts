import * as Router from "koa-router";
import * as bodyParser from "koa-bodyparser";
import { injectable } from "inversify";
import { RouterService } from "../../interfaces/routers/RouterService";
import { errorResponse } from "../../../helpers/ResponseHelper";
import Route from "../../../model/Route";

@injectable()
export abstract class BaseRouter implements RouterService {
    protected static handleError(ctx: Router.IRouterContext, error: any) {
      ctx.status = 500;
      ctx.body = errorResponse("Internal server error", 500);
      ctx.state.error = error;
    }

    protected abstract getRoutes(): Route[];


    public register(router: Router) {
        this.getRoutes().forEach((route) => {
            this.registerRoute(route, router);
        });
    }

    private registerRoute(route: Route, router: Router) {
        router[route.method](route.path, route.action);
    }
}
