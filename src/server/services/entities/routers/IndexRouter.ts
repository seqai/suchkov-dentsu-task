import { injectable } from "inversify";
import { createReadStream } from "fs";
import { BaseRouter } from "./BaseRouter";
import Route from "../../../model/Route";

@injectable()
export class IndexRouter extends BaseRouter {
    protected getRoutes(): Route[] {
        return [
            new Route("/", "get", async (ctx) => {
                ctx.type = "html";
                ctx.body = createReadStream(`./client/index.html`);
            })
        ];
    }
}
