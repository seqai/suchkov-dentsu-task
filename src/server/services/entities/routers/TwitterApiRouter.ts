import { BaseRouter } from "./BaseRouter";
import { injectable, inject } from "inversify";
import { IRouterContext } from "koa-router";
import { Endpoints } from "../../../../common/constants/Endpoints";
import { successResponse } from "../../../helpers/ResponseHelper";
import Route from "../../../model/Route";
import { Identifiers } from "../../../constants/Identifiers";
import TwitterApiService from "../twitter-api/TwitterApiService";


@injectable()
export class TwitterApiRouter extends BaseRouter {
    public constructor(@inject(Identifiers.TwitterApiService) private twitterApiService: TwitterApiService) {
        super();
    }
    protected getRoutes(): Route[] {
        return [
            new Route(`/${Endpoints.Api}/${Endpoints.Twitter}`, "get", async (ctx: IRouterContext) => {
                try {
                  const tweets = await this.twitterApiService.search({
                      hashtags: ctx.request.query["hashtags"] ? ctx.request.query["hashtags"].split(",") : [],
                      includeVideos: ctx.request.query["includeVideos"] === "true"
                  });
                  ctx.status = 200;
                  ctx.body = successResponse(tweets);
                } catch (e) {
                  BaseRouter.handleError(ctx, e);
                }
            })
        ];
    }
}
