import { injectable } from "inversify";
import * as Twit from "twit";
import * as Bluebird from "bluebird";
import { QueryData } from "../../../../common/model/QueryData";
import {generateTwitterQuery} from "../../../../common/helpers/generateTwitterQuery";
import {Tweet} from "../../../../common/model/Tweet";

const TWITTER_APP_CONSUMER_KEY = process.env.TWITTER_APP_CONSUMER_KEY;
const TWITTER_APP_CONSUMER_SECRET = process.env.TWITTER_APP_CONSUMER_SECRET;


@injectable()
export default class TwitterApiService {
    private twit: Twit;
    constructor() {
        this.twit = Bluebird.promisifyAll(new Twit({
            consumer_key: TWITTER_APP_CONSUMER_KEY,
            consumer_secret: TWITTER_APP_CONSUMER_SECRET,
            app_only_auth: true
        }));
    }

    public async search(query: QueryData, count = 10) {
        const data = await this.twit.get(
            "search/tweets",
            { q: generateTwitterQuery(query), count, tweet_mode: "extended" });
        return data.data["statuses"].map(tweet => prepareTweetData(tweet));
    }
}

function prepareTweetData(tweet: any): Tweet {
    const prepared: Tweet = {
        user: tweet["user"]["screen_name"],
        fullName: tweet["user"]["name"],
        avatar: tweet["user"]["profile_image_url"],
        text: tweet["full_text"],
        entities: tweet["entities"]
    };
    if (tweet["extended_entities"] && tweet["extended_entities"]["media"]) {
        const media: any[] = tweet["extended_entities"]["media"];
        prepared.videos = media.filter(m => m.type === "video").map(m => m["expanded_url"]);
        prepared.images = media.filter(m => m.type === "photo").map(m => m["media_url"]);
    } else {
        prepared.videos = [];
        prepared.images = [];
    }
    return prepared;
}

