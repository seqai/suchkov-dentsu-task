import "reflect-metadata";
import { Container } from "inversify";
import { Identifiers } from "../constants/Identifiers";
import ApplicationServer from "../Server";
import { RouterService } from "../services/interfaces/routers/RouterService";
import { IndexRouter } from "../services/entities/routers/IndexRouter";
import { TwitterApiRouter } from "../services/entities/routers/TwitterApiRouter";
import TwitterApiService from "../services/entities/twitter-api/TwitterApiService";

const container = new Container({ defaultScope: "Singleton" });

container.bind<ApplicationServer>(Identifiers.ApplicationServer).to(ApplicationServer).inTransientScope();
container.bind<RouterService>(Identifiers.IndexRouter).to(IndexRouter);
container.bind<RouterService>(Identifiers.TwitterApiRouter).to(TwitterApiRouter);
container.bind<TwitterApiService>(Identifiers.TwitterApiService).to(TwitterApiService);


export { container };
