import * as Koa from "koa";
import * as Router from "koa-router";
import * as bodyparser from "koa-bodyparser";
import * as Bluebird from "bluebird";
import * as servestatic from "koa-static";
import { inject, injectable } from "inversify";
import { Socket, Server } from "net";
import { Identifiers } from "./constants/Identifiers";
import { RouterService } from "./services/interfaces/routers/RouterService";

const TWITTER_APP_PORT =  process.env.TWITTER_APP_PORT || "8081";

@injectable()
export default class ApplicationServer {
    private app: Koa;
    private server: Server;

    private sockets: Map<number, Socket> = new Map<number, Socket>();
    private lastSocketId = 0;

    private isRunning: boolean;

    constructor(@inject(Identifiers.IndexRouter) indexRouter: RouterService,
                @inject(Identifiers.TwitterApiRouter) twitterApiRouter: RouterService) {
        const app = new Koa();
        const router: Router = new Router();
        indexRouter.register(router);
        twitterApiRouter.register(router);

        app.use(servestatic(`./client`));
        app.use(bodyparser());
        app.use(this.errorHandler);
        app.use(router.routes());
        app.use(router.allowedMethods());

        process.on("exit", async () => await this.shutdown());
        process.on("SIGINT", async () => await this.shutdown());
        process.on("SIGUSR1", async () => await this.shutdown());
        process.on("SIGUSR2", async () => await this.shutdown());
        process.on("uncaughtException", async (e) => await this.shutdown(e));

        this.app = app;
    }

    public run(): Server {
        if (this.isRunning) {
            return this.server;
        }
        console.log("Starting Contribution Reporting Service");
        const server = this.app.listen(TWITTER_APP_PORT, () => {
            console.log(`Server listening on port: ${TWITTER_APP_PORT}`);
        });
        this.server = Bluebird.promisifyAll(server);
        this.handleConnections(this.server);

        server.on("close", async () => { console.log("Server close!"); await this.shutdown(); });
        this.isRunning = true;
        return server;
    }

    public async shutdown(error: Error = null): Promise<void> {
        if (!this.isRunning) {
            return;
        }
        this.isRunning = false;

        if (error) {
            console.error("Server critical error:");
            console.error(error);
        }

        console.log("Server is shutting down");
        this.sockets.forEach(socket => {
            socket.destroy();
        });
        this.sockets.clear();
        this.server.removeAllListeners();
        await this.server.close();
        this.server = undefined;
        console.log("Server stopped");
    }

    private handleConnections(server: Server) {
        server.on("connection", (socket: Socket) => {
            this.lastSocketId += 1;
            const socketKey = this.lastSocketId;
            this.sockets.set(socketKey, socket);
            socket.on("close", () => {
                delete this.sockets[socketKey];
            });
        });
    }

    private async errorHandler(ctx: Koa.Context, next: () => Promise<any>) {
        await next();
        if (ctx.state.error) {
            console.log("\r\n\t\x1b[31m------- UNHANDLED ERROR -------\r\n");
            console.error(ctx.state.error);
        }
    }
}
