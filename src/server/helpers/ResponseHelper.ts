import { ServiceResponse } from "../../common/model/ServiceResponse";

export function successResponse<T>(data?: T): ServiceResponse<T> {
    return {
        status: "success",
        data
    };
}

export function failResponse<T>(data: T): ServiceResponse<T> {
    return {
        status: "fail",
        data
    };
}

export function errorResponse(message: string, code?: number): ServiceResponse<void> {
    return {
        status: "error",
        message,
        code
    };
}
