const Identifiers = {
    ApplicationServer: Symbol("ApplicationServer"),
    IndexRouter: Symbol("IndexRouter"),
    TwitterApiRouter: Symbol("TwitterApiRouter"),
    TwitterApiService: Symbol("TwitterApiService")
};

export { Identifiers };
