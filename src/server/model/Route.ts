import { IRouterContext } from "koa-router";

export default class Route {
    constructor (public path: string, public method: string, public action: (ctx: IRouterContext) => void) {}
}
