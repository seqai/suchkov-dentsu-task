import { NgModule } from "@angular/core";
import {
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatTooltipModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatBottomSheetModule,
    MatTabsModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatProgressSpinnerModule,
    MatSnackBarModule
} from "@angular/material";

@NgModule({
    exports: [
        MatSidenavModule,
        MatToolbarModule, MatListModule,
        MatCardModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule,
        MatTooltipModule,
        MatSelectModule,
        MatSlideToggleModule,
        MatCheckboxModule,
        MatBottomSheetModule,
        MatTabsModule,
        MatButtonToggleModule,
        MatChipsModule,
        MatProgressSpinnerModule,
        MatSnackBarModule
    ]
})
export class AppMaterialModule { }
