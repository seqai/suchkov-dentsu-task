import { Component, Input, OnInit } from "@angular/core";
import { Tweet } from "../../../../common/model/Tweet";
import { DomSanitizer} from "@angular/platform-browser";
import { SafeStyle } from "@angular/platform-browser/src/security/dom_sanitization_service";

@Component({
  selector: "app-tweet-item",
  templateUrl: "./tweet-item.component.html",
  styleUrls: ["./tweet-item.component.css"]
})
export class TweetItemComponent implements OnInit {
    @Input() tweet: Tweet;
    public constructor(private sanitizer: DomSanitizer) { }

    public ngOnInit(): void {
    }

    public getBackground(image: string): SafeStyle {
        return this.sanitizer.bypassSecurityTrustStyle(`url(${image})`);
    }
}
