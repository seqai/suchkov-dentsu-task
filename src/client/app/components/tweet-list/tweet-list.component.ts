import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from "@angular/core";
import { QueryPanelComponent } from "../query-panel/query-panel.component";
import { EMPTY, fromEvent, merge, Observable, of, Subject } from "rxjs";
import { catchError, map, switchMap, take, takeUntil, tap } from "rxjs/operators";
import { TwitterApiServiceService } from "../../service/twitter-api-service.service";
import { ServiceResponse } from "../../../../common/model/ServiceResponse";
import { MatSnackBar} from "@angular/material";
import { successResponse} from "../../../../server/helpers/ResponseHelper";
import { QueryData } from "../../../../common/model/QueryData";
import {Tweet} from "../../../../common/model/Tweet";

@Component({
  selector: "app-tweet-list",
  templateUrl: "./tweet-list.component.html",
  styleUrls: ["./tweet-list.component.css"]
})
export class TweetListComponent implements OnInit, AfterViewInit, OnDestroy {
    private ngUnsubscribe: Subject<any> = new Subject();
    @ViewChild("queryPanel") private queryPanel: QueryPanelComponent;

    public tweet$: Observable<Tweet[]>;
    public empty$: Observable<boolean>;
    constructor(private api: TwitterApiServiceService, private snackBar: MatSnackBar) { }

    private handleRequestError(): void {
        this.snackBar.open("Error fetching tweet$", "Ok", { duration: 3000 });
    }

    public ngOnInit(): void {
        this.queryPanel.add(["dentsuaegis"]);
        this.tweet$ = merge(of<QueryData>(this.queryPanel.query), this.queryPanel.queryChangeEvent.asObservable())
            .pipe(
                takeUntil(this.ngUnsubscribe),
                tap(q => this.queryPanel.showIndicator = q.hashtags.length > 0),
                switchMap(q => q.hashtags.length === 0 ? of<ServiceResponse<Tweet[]>>(successResponse([])) :
                    this.api.getTweets(q)
                    .pipe(catchError( err => {
                    console.log("Request error");
                    console.log("Error");
                    return of<ServiceResponse<Tweet[]>>(undefined);
                }))),
                tap(() => this.queryPanel.showIndicator = false),
                map(d => {
                    if (d !== undefined && d.status === "success") {
                        return d.data;
                    } else {
                        this.handleRequestError();
                        return [];
                    }
                }));
        this.empty$ = this.queryPanel.queryChangeEvent.pipe(map(q => q.hashtags.length === 0));

    }

    public ngAfterViewInit(): void {
        this.queryPanel.add(["dentsuaegis"]);
    }

    public ngOnDestroy(): void {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
