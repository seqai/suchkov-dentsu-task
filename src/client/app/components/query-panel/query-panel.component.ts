import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from "@angular/core";
import { MatButton, MatCheckbox, MatChipInputEvent, MatChipList } from "@angular/material";
import { COMMA, ENTER, SPACE } from "@angular/cdk/keycodes";

import { QueryData } from "../../../../common/model/QueryData";
import { merge, fromEvent, Subject } from "rxjs";
import { map, takeUntil } from "rxjs/operators";

@Component({
  selector: "app-query-panel",
  templateUrl: "./query-panel.component.html",
  styleUrls: ["./query-panel.component.css"]
})
export class QueryPanelComponent implements OnInit, OnDestroy {
    private ngUnsubscribe: Subject<any> = new Subject();
    private chipChanges = new EventEmitter();
    public readonly separatorKeysCodes: number[] = [ENTER, COMMA, SPACE];
    public hashtags = [];

    @Input()  public showIndicator: boolean;
    @Output() public queryChangeEvent = new EventEmitter<QueryData>();
    @Output() public query: QueryData = { hashtags: this.hashtags, includeVideos: false };

    @ViewChild("listHashtags") private listHashtags: MatChipList;
    @ViewChild("checkboxVideos") private checkboxVideos: MatCheckbox;
    @ViewChild("buttonRefresh") private buttonRefresh: MatButton;

    public addChip(event: MatChipInputEvent): void {
        const input = event.input;
        const hashtags = (event.value || "")
            .trim()
            .split("#");
        this.add(hashtags);

        if (input) {
            input.value = "";
        }
    }

    public add(hashtags: string[]) {
        const newHashtags = hashtags.filter(v => v && this.hashtags.indexOf(v) === -1);
        newHashtags.forEach(v => this.hashtags.push(v));
        if (newHashtags.length > 0) {
            this.chipChanges.emit();
        }
    }

    public remove(hashtag: string): void {
        const index = this.hashtags.indexOf(hashtag);
        if (index >= 0) {
            this.hashtags.splice(index, 1);
            this.chipChanges.emit();
        }
    }

    public ngOnInit(): void {
        merge(
            this.chipChanges,
            this.checkboxVideos.change,
            fromEvent(this.buttonRefresh._getHostElement(), "click"))
        .pipe(
            takeUntil(this.ngUnsubscribe),
            map(_ => this.getHashtags(this.hashtags, this.checkboxVideos.checked))
        ).subscribe(
            q => this.queryChangeEvent.emit(q)
        );
    }

    private getHashtags(hashtags: string[], includeVideos: boolean): QueryData {
        return { hashtags, includeVideos };
    }

    public ngOnDestroy(): void {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
