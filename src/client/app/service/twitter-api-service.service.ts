import { Injectable } from "@angular/core";
import { QueryData } from "../../../common/model/QueryData";
import { Endpoints } from "../../../common/constants/Endpoints";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { ServiceResponse } from "../../../common/model/ServiceResponse";

const API_URL = `/${Endpoints.Api}/${Endpoints.Twitter}`;

@Injectable({
  providedIn: "root"
})
export class TwitterApiServiceService {
    constructor(private httpClient: HttpClient) {}
    public getTweets(query: QueryData): Observable<ServiceResponse<any>> {
        const queryItems = {
            hashtags: query.hashtags.map(hashtag => encodeURIComponent(hashtag)).join(","),
            includeVideos: query.includeVideos
        };
        const queryString = Object.keys(queryItems)
            .filter(k => queryItems[k])
            .map(k => `${k}=${queryItems[k]}`)
            .join("&");
        return this.httpClient.get<ServiceResponse<any>>(`${API_URL}${queryString ? `?${queryString}` : ""}`);
    }
}
