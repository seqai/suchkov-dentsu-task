// Based on https://stackblitz.com/edit/linkify-pipe
import { Pipe, PipeTransform } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";

@Pipe({
    name: "linkify"
})
export class LinkifyPipe implements PipeTransform {

    constructor(private _domSanitizer: DomSanitizer) {}

    private static stylize(text: string): string {
        let stylizedText = "";
        if (text && text.length > 0) {
            for (const t of text.split(" ")) {
                if (t.startsWith("@") && t.length > 1) {
                    stylizedText += `<a href="https://twitter.com/${t.substring(1)}">${t}</a> `;
                } else if (t.startsWith("#") && t.length > 1) {
                    stylizedText += `<a href="https://twitter.com/hashtag/${t.substring(1)}">${t}</a> `;
                } else if (t.startsWith("http://") || t.startsWith("https://") || t.startsWith("www.")) {
                    stylizedText += `<a href="${t}">${t}</a> `;
                } else {
                    stylizedText += t + " ";
                }
            }
            return stylizedText;
        } else {
            return text;
        }
    }

    public transform(value: any, args?: any): any {
        return this._domSanitizer.bypassSecurityTrustHtml(LinkifyPipe.stylize(value));
    }
}
