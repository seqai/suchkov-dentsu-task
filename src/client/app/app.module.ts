import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { AppMaterialModule } from "./shared/app-material/app-material.module";
import { TweetListComponent } from "./components/tweet-list/tweet-list.component";
import { QueryPanelComponent } from "./components/query-panel/query-panel.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { TwitterApiServiceService } from "./service/twitter-api-service.service";
import { HttpClientModule } from "@angular/common/http";
import { TweetItemComponent } from "./components/tweet-item/tweet-item.component";
import { LinkifyPipe } from "./pipes/linkify.pipe";

@NgModule({
    declarations: [
        AppComponent,
        TweetListComponent,
        QueryPanelComponent,
        TweetItemComponent,
        LinkifyPipe
    ],
    imports: [
        BrowserModule,
        AppMaterialModule,
        BrowserAnimationsModule,
        HttpClientModule
    ],
    providers: [TwitterApiServiceService],
    bootstrap: [AppComponent]
})
export class AppModule { }
