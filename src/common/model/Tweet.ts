
export interface Tweet {
    user: string;
    fullName: string;
    avatar: string;
    text: string;
    entities: { [key in entitiyType]: TweetEntity[] };
    images?: string[];
    videos?: string[];
}

type entitiyType = "hashtags" | "symbols" | "user_mentions" | "urls";

interface TweetEntity {
    text: string;
}
