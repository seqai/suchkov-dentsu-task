// JSEND service response
// http://labs.omniti.com/labs/jsend

export interface ServiceResponse<T> {
    status: "success" | "fail" | "error";
    data?: T;
    message?: string;
    code?: number;
}
