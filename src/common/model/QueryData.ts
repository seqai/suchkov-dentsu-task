export interface QueryData {
    hashtags: string[];
    includeVideos: boolean;
}
