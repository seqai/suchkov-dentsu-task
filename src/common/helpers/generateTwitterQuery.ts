import {QueryData} from "../model/QueryData";

export function generateTwitterQuery(query: QueryData): string {
    const tokens = query.hashtags.map(h => `#${h}`);
    if (!query.includeVideos) {
        tokens.push("-filter:native_video");
        tokens.push("-filter:periscope");
        tokens.push("-filter:vine");
    }
    return tokens.join(" ");
}
